#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _WIN32
#include <windows.h>
#include <winsock.h>

#define CLOSE_SOCK(SOCK) closesocket(SOCK)
#else
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

#define SOCKET int
#define SOCKET_ERROR -1
#define CLOSE_SOCK(SOCK) close(SOCK)
#endif

/*
    Code which has to be executed before first socket use; needed for windows compatibility
*/
int socketInitBeforeFirstUse()
{
    int retval = 0;
    #ifdef _WIN32
        WSADATA wsaData;
        WSAStartup(MAKEWORD(2,0),&wsaData);
    #else

    #endif
    return retval;
}

/*
    Code which has to be executed after end of last socket use; needed for windows compatibility
*/
int socketExitCode()
{
    int retval = 0;
    #ifdef _WIN32
        WSACleanup();
    #else

    #endif
    return retval;
}

/*
    Handles occuring errors
    @param errorString is String which is printed before quit
*/
void errorHandler(char* errorString)
{
    printf("%s\n",errorString);
    exit(1);
}

/*
    Funtion for translation an sockaddr Struck to a human readablestring
    @param string is a point to the Buffer in which the converted string is stored. 16 Bytes should be sufficient
    @param addressStruct is the struct which contains the IP that should be converted
    @return returns the pointer which was in the function call for easier usage in conjunction with printf
*/
char* sockaddrToIpString(char * string, struct sockaddr_in * addressStruct)
{
    //char string[16];
    #ifdef _WIN32
        //char string[16] = 0;
        sprintf(string,"%d.%d.%d.%d",
                addressStruct->sin_addr.S_un.S_un_b.s_b1,
                addressStruct->sin_addr.S_un.S_un_b.s_b2,
                addressStruct->sin_addr.S_un.S_un_b.s_b3,
                addressStruct->sin_addr.S_un.S_un_b.s_b4
                );
    #else
        sprintf(string, "%d.%d.%d.%d",
                (addressStruct->sin_addr.s_addr)&0xff,
                (addressStruct->sin_addr.s_addr>>8)&0xff,
                (addressStruct->sin_addr.s_addr>>16)&0xff,
                (addressStruct->sin_addr.s_addr>>24)&0xff
                );
    #endif
    return string;
}

int main(int argc, char* argv[])
{
    int port = 7;
    if (argc<2){
        printf("No Port specified! Defaulting to port %d\nCall like: udpEchoServer <Portnumber>\n",port);
    }
    else {
        port = atoi(argv[1]);
    }


    socketInitBeforeFirstUse();
    struct sockaddr_in server_addres, client_address;
    char buffer[1024] = {0};
    char ipString[16] = {0};

    SOCKET serverSock = socket(AF_INET,SOCK_DGRAM,0);
    int n = 0;
    if (serverSock == SOCKET_ERROR) errorHandler("Error at creation of socket!");

    memset(&server_addres, 0, sizeof(server_addres));
    server_addres.sin_family = AF_INET;
    server_addres.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addres.sin_port = htons(port);

    if (bind(serverSock, (struct sockaddr*)&server_addres, sizeof(server_addres))) errorHandler("Error at binding of Socket");

    printf("Echo Server started at Port %d ...\n", port);

    //if (listen(serverSock, 5)<0) errorHandler("Error at listening for connections");
    while(1)
    {
        //SOCKET client_sock = accept(serverSock,(struct sockaddr*)&client_address, &sizeof(client_address));
        //if (client_sock < 0) errorHandler("Error at Establishing of connection")
        memset(buffer, 0, sizeof(buffer));
        int addrLen = sizeof(client_address);
        n = recvfrom(serverSock, buffer, sizeof(buffer), 0, (struct sockaddr*)&client_address, &addrLen);
        printf("Connection from %s established!\nPayload:\n%s\n",
                sockaddrToIpString(ipString,&client_address),
                buffer);
        sendto(serverSock, buffer, n, 0, (struct sockaddr*)&client_address, sizeof(client_address));
    }

    socketExitCode();
}
